'use strict'

var express=require('express'),
	favicon=require('serve-favicon'),
	bodyParser=require('body-parser'),
	morgan=require('morgan'),
	restFul=require('express-method-override')('_method'),
	jade=require('jade'),
	routes=require('./routes/movie-router'),
	faviconURL=`${__dirname}/public/img/apple-touch-icon.png`,
	publicDir=express.static(`${__dirname}/public`),
	viewDir=`${__dirname}/views`,
	port=(process.env.PORT || 3000),
	app=express()

app
	.set('views',viewDir)
	.set('view engine','jade') //motor de plantillas
	.set('port',port)	
	.use(favicon(faviconURL))
	.use(bodyParser.json())//parsea a formato json
	.use(bodyParser.urlencoded({extended:false}))// application/x-www-form-urlencoded
	.use(restFul)
	.use(morgan('dev'))
	.use(publicDir)
	.use(routes)//.use('/',routes)

module.exports=app