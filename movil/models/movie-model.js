'use strict'

var conn=require('./movie-connection'),
	MovieModel=()=>{}
	
MovieModel.getAll=(cb)=> conn.query('SELECT * FROM movie',cb)

MovieModel.getOne=(id,cb)=> conn.query('SELECT * FROM movie WHERE movie_id = ?', id, cb)

MovieModel.insert=(data,cb)=> conn.query('INSERT INTO movie SET ?', data, cb)

MovieModel.update=(data,id,cb)=> conn.query('UPDATE movie SET ? WHERE movie_id = ?', [data,id], cb)

MovieModel.delete=(id,cb)=> conn.query('DELETE FROM movie WHERE movie_id = ?', id, cb)

MovieModel.save=(data,id,cb)=>{
	conn.query('SELECT * FROM movie WHERE movie_id = ?',id,(err,rows)=>{
		console.log(`Numero de registros: ${rows.length}`)
		if(err){
			return err
		}else{
			return (rows.length==1)
				? conn.query('UPDATE movie SET ? WHERE movie_id = ?', [data,id], cb)
				: conn.query('INSERT INTO movie SET ?', data, cb)
		}
	})
} 

MovieModel.close=()=> conn.end()

module.exports=MovieModel