function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: myLatlng
    });
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map);
}

function geocodeAddress(geocoder, resultsMap) {
    var address = s; //document.getElementById('address').value;
    geocoder.geocode({
        'address': address
    }, function (results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location,
                animation: google.maps.Animation.DROP,
                title: "Ubicación referencial al punto real.",
                icon: image
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

$(function () {
    $("#btn_map_abrir").on("click", function (e) {
        e.preventDefault();
        $('#map').prependTo('#map_amplio').animate({
            'position': 'absolute',
            'height': '100%',
            'width': '100%'
        }, "fast", function () {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
    });
    $("#btn_map_cerrar").on("click", function (e) {
        e.preventDefault();
        $('#map').prependTo('#map_normal').animate({
            'height': '200px',
            'width': '100%'
        }, "fast", function () {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
    });
});