/*document.cancelFullScreen = document.webkitCancelFullScreen ||
    document.mozCancelFullScreen;

var elem = document.querySelector(document.webkitCancelFullScreen ? "#fs" : "#fs-container");

function onFullScreenEnter() {
    console.log("Entered fullscreen initiated from iframe!");
    elem.onwebkitfullscreenchange = onFullScreenExit;
    elem.onmozfullscreenchange = onFullScreenExit;
};

// Called whenever the browser exits fullscreen.
function onFullScreenExit() {
    console.log("Exited fullscreen initiated from iframe");
};

// Note: FF nightly needs about:config full-screen-api.enabled set to true.
function enterFullscreen() {
    console.log("enterFullscreen()");
    elem.onwebkitfullscreenchange = onFullScreenEnter;
    elem.onmozfullscreenchange = onFullScreenEnter;
    if (elem.webkitRequestFullScreen) {
        elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    } else {
        elem.mozRequestFullScreen();
    }
    document.getElementById('enter-exit-fs').onclick = exitFullscreen;
}

function exitFullscreen() {
    console.log("exitFullscreen()");
    document.cancelFullScreen();
    document.getElementById('enter-exit-fs').onclick = enterFullscreen;
}*/

function toggleFullScreen(elem) {
    var btn=$("#enter-exit-fs");
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {        
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
        btn.find("i").removeClass("fa-window-maximize").addClass("fa-window-restore"); 
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        btn.find("i").removeClass("fa-window-restore").addClass("fa-window-maximize"); 
    }
}