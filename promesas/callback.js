'use strict'

var fs = require('fs'),
	file = './assets/nombres.txt',
	newFile = './assets/nombres-callback.txt'

/*
fs.exists('assets/nombres.txt', function(exists){
	if(exists){
		console.log('Archivo existe')
	}else{
		console.log('Archivo no existe')
	}
})
*/
fs.access(file, fs.F_OK, function(err){
	if(err){
		console.log('Archivo no existe')
	}else{
		console.log('Archivo existe')
		fs.readFile(file, function(err, data){
			if(err){
				console.log('Archivo no se pudo leer')
			}else{
				console.log('Archivo se ha leido exitosamente')
				fs.writeFile(newFile, data, function(err){
					return (err)?console.log('El archivo no se pudo copiar'):console.log('El archivo se ha copiado con exito')
				})
			}
		})
	}
})