'use strict'

var fs = require('fs'),
	file = './assets/nombres.txt',
	newFile = './assets/nombres-promesas-q.txt',
	q = require('q')

function existFile(file){
	let defer = q.defer()	
	fs.access(file, fs.F_OK, function(err){
		return (err) ? defer.reject(new Error('El archivo no existe')) : defer.resolve(true)
	})
	return defer.promise
}
function readFile(file){
	let defer = q.defer()
	console.log('Archivo existe')
	fs.readFile(file, function(err, data){
		return (err) ? defer.reject(new Error('El archivo no se pudo leer')) : defer.resolve(data)
	})
	return defer.promise
}
function writeFile(file,data){
	let defer = q.defer()
	console.log('Archivo se ha leido exitosamente')
	fs.writeFile(file, data, function(err){
		return (err) ? defer.reject(new Error('El archivo no se pudo copiar')) : defer.resolve('El archivo se ha copiado con exito')
	})
	return defer.promise
}
/*
Si existe el archivo
 Abrelo
 Leelo
 Copialo
 Avisa q se opoio
 Maneja errores
*/
existFile(file)
	.then(function(){ return readFile(file) })
	.then(function(dataPromise){ return writeFile(newFile,dataPromise) })
	.then(function(dataPromise){ return console.log(dataPromise) })
	.fail(function(err){ return console.log(error.message) })