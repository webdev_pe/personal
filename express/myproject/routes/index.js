'use strict'

var express=require('express'),
	router=express.Router()

function jade(req, res, next){
	let locals={
			title:'Jade',
			link:'http://jadelang.net/',
			description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem hic eaque tenetur necessitatibus incidunt, laborum facere? Accusamus reiciendis error illum libero qui, quibusdam unde fuga excepturi accusantium aspernatur eveniet assumenda.'
		}
	res.render('index',locals)
}

function error404(req, res, next){
	let error=new Error(),
		locals={
			title:'Error 404',
			description:'Recurso no encontrado',
			error:error
		}
	error.status=404
	res.render('error',locals)
	next()
}

router
	.get('', (req,res)=>{
		res.end('<h1>Terminamos la configuraci&oacute;n App en Express</h1>')
	})
	.get('/jade', jade)
	.use(error404)

module.exports=router