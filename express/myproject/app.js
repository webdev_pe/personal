'use strict'

var express=require('express'),
	favicon=require('serve-favicon'),
	morgan=require('morgan'),
	jade=require('jade'),
	routes=require('./routes/index'),
	faviconURL=`${__dirname}/public/img/apple-touch-icon.png`,
	publicDir=express.static(`${__dirname}/public`),
	viewDir=`${__dirname}/views`,
	port=(process.env.PORT || 3000),
	app=express()

app
	//configurando app
	.set('views',viewDir)
	.set('view engine','jade') //jade, ejs
	.set('port',port)
	//ejecutando midlewares
	.use(favicon(faviconURL))
	.use(morgan('dev'))
	.use(publicDir)
	//ejecuto el midleware enrutador
	.use('/',routes)

module.exports=app