/*'use strict'
const http=require('http')
function webServer(req,res){
	res.writeHead(200,{'Content-Type':'text/html'})
	res.end('<h1>NodeJsServer</h1>')
}
http.createServer(webServer).listen(3850,'localhost')
console.log('Servidor NodeJs Running in Server http://localhost:3850')*/
'use strict'
var http=require('http'),
	util=require('util'),
	formidable=require('formidable'),
	fse=require('fs-extra')
function webServer(req,res){
	if(req.method.toLowerCase()=='get' && req.url=='/'){
		let form=`
			<h1>Subir archivos</h1>
			<form action="/upload" enctype="multipart/form-data" method="post">
				<div><input type="file" name="upload" required></div>
				<div><input type="submit" value="Subir archivo"></div>
			</form>
		`
		res.writeHead(200,{'Content-Type':'text/html'})
		res.end(form)
	}
	if(req.method.toLowerCase()=='post' && req.url=='/upload'){
		let form=formidable.IncomingForm()
		form
			.parse(req,function(err,fields,files){
				res.writeHead(200,{'Content-Type':'text/html'})
				res.write(`
					<h1>Archivos recibidos</h1>
					<a href="/">regresar</a>
					${ util.inspect({files:files}) }
					`
				)//usar cuando hay mas volumen de información				
				res.end()
			})
			.on('progress',function(bytesReceived,bytesExpected){
				let percentCompleted=(bytesReceived/bytesExpected)*100
				console.log(percentCompleted.toFixed(2))
			})//emisor de eventos
			.on('error',function(err){
				console.log(err)
			})
			.on('end',function(fields,files){
				 	//ubicacion temporal del archivo
				let temPath=this.openedFiles[0].path,
				 	//nombre del archivo
					fileName=this.openedFiles[0].name,
					//nueva ubicacion
					newLocation='./upload/'+fileName
				fse.copy(temPath,newLocation,function(err){
					return (err)?console.log(err):console.log('El archivo se subio con exito')
				})
			})
		return
	}
}
http.createServer(webServer).listen(3850,'localhost')
console.log('Servidor NodeJs Running in Server http://localhost:3850')